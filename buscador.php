
<?php
	include 'functions/funciones.php';	
?>

<html>
	<head>
		<title> Single Blog </title>
		<link href="css/style.css" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>

	<body>		
		<?php include "partes/cabecera.php"; ?>

		<?php include "partes/enlaces.php"; ?>

		<div id ="main">
			<section>
				<article>

					<div class="big">						
						<form action="" method="POST" id="login">							
							<input name="busca" placeholder="" autocomplete="off" type="text" />	
							<input type="submit" class="button rouded" value="Search" />	
						</form>
					</div>

				</article>				
						
				<?php
					if(isset($_POST['busca'])){
						$busca = $_POST['busca'];
						buscar($busca);
					}

				?>
			</section>
			<aside>												
				<article>
					<?php ultimasEntradas(); ?>
				</article>					
			</aside>
		</div>

	</body>
</html>