
<?php
	$envio = $_GET['id_post'];
	include 'functions/funciones.php';	
?>

<html>
	<head>
		<title> Single Blog </title>
		<link href="css/style.css" rel="stylesheet">
	</head>

	<body>

		<?php include "partes/cabecera.php"; ?>	
		
		<?php include "partes/enlaces.php"; ?>

		<div id="main">			
			<section>				
				<?php listarPost($envio); ?>
			</section>	

			<aside>												
				<article>
					<?php ultimasEntradas(); ?>
				</article>					
			</aside>			
		</div>
	</body>
</html>