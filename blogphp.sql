-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-07-2014 a las 09:20:40
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `blogphp`
--
CREATE DATABASE IF NOT EXISTS `blogphp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `blogphp`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `contenido` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `titulo`, `contenido`) VALUES
(1, 'Prueba de Contenido', 'Este es un contenido que se guarda en la base de datos y me sirve para saber si la Select esta bien hecha o no ;)'),
(2, 'Entrada con codigo HTML', 'Prueba de <br>codigo HTML</br> ahora algo con el texto <center> centrado </center> <p> probamos con un parrafo </p> <p> y puede que con una imagen</p> <img src="images/microsoft.png" alt="imagen">'),
(3, 'Lorem Ipsum Dolor', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis vulputate sem, in laoreet eros. Nunc facilisis dolor eu mi ornare ultrices. Etiam a tincidunt libero. Nam sodales mattis urna id tempor. Pellentesque eleifend diam felis, ut lobortis ante fringilla ut. Fusce semper quam ac velit molestie, eget fringilla metus vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi fringilla sem et elit varius egestas. Suspendisse turpis'),
(4, 'Mas codigo para probar', '<iframe width="640" height="360" src="//www.youtube.com/embed/i4RE6dBAjH4" frameborder="0" allowfullscreen></iframe>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
