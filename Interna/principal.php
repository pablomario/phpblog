
<?php
	include '../functions/funciones.php';

?>

<html>
	<head>
		<title> Blog </title>
		<link href="../css/style.css" rel="stylesheet">
	</head>

	<body>
		<div id="main">			
			<?php include "../partes/cabecera.php"; ?>
			<div id ="contenedor">
							
				<nav>
					<ul>
						<a href="principal.php"><li>Home</li></a>
						<a href="nueva_entrada.php"><li>New Post</li></a>
						<a href="../buscador.php"><li>Search</li></a>
						<a href="../logout.php"><li>LogOut</li></a>						
					</ul>
				</nav>				

				<div id="caja">
					<div id="cabecera">
						<h2>Private Zone</h2>
					</div>

					<!-- CONTENEDOR DE CADA POST -->
					<?php listarTitulos(); ?>
					
					
					<?php include "../partes/enlaces.php"; ?>
				</div>

			</div>					
		</div>

	</body>
</html>