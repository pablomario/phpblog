
<?php
	
	include '../functions/funciones.php';	
?>

<html>
	<head>
		<title> Single Blog </title>
		<link href="../css/style.css" rel="stylesheet">
	</head>

	<body>
		<div id="main">
			<?php include "../partes/cabecera.php"; ?>
			<div id ="contenedor">	
						
				<nav>
					<ul>
						<a href="principal.php"><li>Home</li></a>
						<a href="nueva_entrada.php"><li>New Post</li></a>
						<a href="../buscador.php"><li>Search</li></a>
						<a href="../logout.php"><li>LogOut</li></a>						
					</ul>
				</nav>	
				
				<div id="caja">
					
					<form action="" method="POST" id="login">							
						<input name="titulo" id="titulo" placeholder="titulo" autocomplete="off" type="text" />	
						<input name="contenido" id="contenido" placeholder="contenido" autocomplete="off" type="text" />	
						<input name="fecha" id="fecha" placeholder="fecha" autocomplete="off" />				
						<input type="submit" class="button" value="LogIn" />	
					</form>	

					<?php

						if((isset($_POST['titulo'])) && (isset($_POST['contenido']))
							&& !empty($_POST['titulo']) && !empty($_POST['contenido'])){

							if(insertEntrada($_POST['titulo'],$_POST['contenido'],$_POST['fecha'])){
								echo "<h2>Insertado con Exito</h2>";
							}else{
								echo "<h2>Errror al insertar</h2>";
							}

						}else{									
							echo "<h2>Campos vacios</h2>";
						}
						
					?>		
					
				</div>

			</div>			
		</div>

	</body>
</html>