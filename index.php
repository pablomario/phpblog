
<?php
	include 'functions/funciones.php';		
?>

<html>
	<head>
		<title> Your Official Enterprise </title>
		<link href="css/style.css" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

		<script type="text/javascript">

			$(document).ready(function(){
				$('#publi').click(function(){
					$('#publi').css('display','none');
				});

			});

		</script>

	</head>

	<body>

		<?php include "partes/cabecera.php"; ?>	
		
		<?php include "partes/enlaces.php"; ?>

		<div id="main">			
			
				
			<section>
				<!-- CONTENEDOR DE CADA POST -->			
				<?php listarEntradasIndex(); ?>				
			</section>

			<aside>												
				<article>
					<?php ultimasEntradas(); ?>
				</article>					
			</aside>
			
		</div>

		<?php include "partes/pie.php"; ?>	
	

	</body>
</html>